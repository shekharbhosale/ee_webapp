package com.extraaedge.web.appconfig;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import com.extraaedge.web.datafiledrivers.ExcelOperations;
import io.github.bonigarcia.wdm.ChromeDriverManager;
import io.github.bonigarcia.wdm.FirefoxDriverManager;


public class ConfigMethods  
{
	public static WebDriver driver = null;
	
	public static WebDriver OpenBrowser (int _tcRow)
	{
		String _browserName;
		try
		{
//			_tcRow=_tcRow-1;
			_browserName = ExcelOperations.readCellValue(_tcRow, Constants.BrowserType);
			if(_browserName.equals("Chrome"))
				{
					ChromeDriverManager.getInstance().setup();
					driver = new ChromeDriver();
					Reporter.log("Chrome Browser is opened",true);
					driver.get(Constants.PlatformURL);
					driver.manage().window().maximize();
					Reporter.log("Browser is maximized.",true);
				}
			else if(_browserName.equals("Mozilla"))
			try {
					FirefoxDriverManager.getInstance().setup();
					driver = new FirefoxDriver();
					Reporter.log("Firefox Browser opened",true);
					driver.get(Constants.PlatformURL);		
					driver.manage().window().maximize();
					Reporter.log("ExtraaEdge platform login page is displayed",true);
				} 
				catch (Exception e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return driver;
	}
	
	public static String getTCName(String _tcName)
	{
		String tcName = _tcName;
		int posi = tcName.indexOf("@");
		tcName = tcName.substring(0, posi);
		posi = tcName.lastIndexOf(".");	
		tcName = tcName.substring(posi+1);
		return tcName;
	}
	public static void waitForPageToLoad() 
	{
        ExpectedCondition<Boolean> expectation = new
                ExpectedCondition<Boolean>() {
                    public Boolean apply(WebDriver driver) {
                        return ((JavascriptExecutor) driver).executeScript("return document.readyState").toString().equals("complete");
                    }
                };
        try {
//            Thread.sleep(3000);
            WebDriverWait wait = new WebDriverWait(driver, 30);
            wait.until(expectation);
        } catch (Throwable error) { }
    }
	
	
}
