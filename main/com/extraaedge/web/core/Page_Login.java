package com.extraaedge.web.core;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Reporter;
import com.extraaedge.web.appconfig.Constants;
import com.extraaedge.web.appconfig.ObjectMapping;

// TODO: Auto-generated Java doc
/**
 * The Class LoginPage.
 */
public class Page_Login extends AppDriver
{
	private static WebElement element;
	public static ObjectMapping properties = new ObjectMapping(Constants.LOCATORS);	
	
	public Page_Login(WebDriver appDriver) 
	{
		super(appDriver);
	}
	
	/**
	 * Display background image login page.
	 *
	 * @throws Exception the exception
	 */
	public static WebElement displayBackgroundImageLoginPage() throws Exception
	{
		return driver.findElement(properties.selectLocator("staging_loginpage_backgroundImage"));
	}

	/**
	 * User name.
	 * @param string 
	 * @return the web element
	 * @throws Exception 
	 */
	public static WebElement displayUsernameInputBox() throws Exception
	{
			element = driver.findElement(properties.selectLocator("staging_username_xpath"));	
			if(element.isDisplayed())
				Reporter.log("Username input box is displayed.",true);
			else
				Reporter.log("Username input box is not displayed.",true);
			return element;
	}
	
	/**
	 * Password.
	 * @param string 
	 * @return the web element
	 * @throws Exception 
	 */
	public static WebElement displayPasswordInputBox() throws Exception
	{
		element = driver.findElement(properties.selectLocator("staging_password_xpath"));
		if(element.isDisplayed())
			Reporter.log("Password input box is displayed.",true);
		else
			Reporter.log("Password input box is not displayed.",true);
		return element;
	}
	
	/**
	 * Click login button.
	 * @throws Exception 
	 */
	public static WebElement clickOnLoginButton() throws Exception
	{
		element = driver.findElement(properties.selectLocator("staging_loginButton_xpath"));
		if(element.isDisplayed())
			Reporter.log("Login button is displayed.",true);
		else
			Reporter.log("Login button is not displayed.",true);
		return element;
	}
	
	public static String getLoginPageURL()
	{
		return driver.getCurrentUrl();
	}
	
	public static WebElement displayUsernamePlaceholder() throws Exception
	{
		return driver.findElement(properties.selectLocator("staging_loginpage_usernamePlaceholder"));
	}

	public static WebElement displayClientAccountLogo() throws Exception
	{
		return driver.findElement(properties.selectLocator("staging_loginpage_accountlogo"));
	}
}
