package com.extraaedge.web.core;

import org.testng.Reporter;
import com.extraaedge.web.appconfig.Constants;
import com.extraaedge.web.datafiledrivers.ExcelOperations;

public class Action_Login 
{
	public static void performLoginAction(int _tcRow) throws Exception
	{
		//Validate Login Page URL
		try {
			if(Constants.PlatformURL.equals(Page_Login.getLoginPageURL()))
				Reporter.log("User has opened correct login page.",true);
		} catch (Exception e) {
			Reporter.log("User has opened incorrect login page.",true);
		}
		
		//Validate Background Image on Login Page
		try {
			if(Page_Login.displayBackgroundImageLoginPage().isDisplayed())
				Reporter.log("Background Image on Login Page is displayed",true);
		} catch (Exception e) {
			Reporter.log("Background Image on Login Page is not displayed",true);
		}
		
		//Validate Account Logo is displayed
		try {
			if(Page_Login.displayClientAccountLogo().isDisplayed())
				Reporter.log("Account Logo is displayed.",true);
		} catch (Exception e) {
			Reporter.log("Account Logo is not displayed.",true);
			throw(e);
		}
		
		//Validate Username Field
		try {
			String _userName = ExcelOperations.readCellValue(_tcRow, Constants.Username);
			Page_Login.displayUsernameInputBox().sendKeys(_userName);
			Reporter.log("Username entered successfully.",true);
			
		} catch (Exception e) {
			Reporter.log("User has failed to enter correct username.",true);
		}
		
		
		//Validate Password Field
		try {
			String _passWord = ExcelOperations.readCellValue(_tcRow, Constants.Password);
			Page_Login.displayPasswordInputBox().sendKeys(_passWord);
			Reporter.log("Password entered successfully.",true);
		} catch (Exception e) {
			Reporter.log("User has failed to enter correct password.",true);
		}
		
		//Validate Login Button
		try {
			Page_Login.clickOnLoginButton().click();
			Reporter.log("User has clicked Log In button.",true);
		} catch (Exception e) {
			Reporter.log("User is unable to click Log In button.",true);
		}
		
		Thread.sleep(3000);
			
	}

}
