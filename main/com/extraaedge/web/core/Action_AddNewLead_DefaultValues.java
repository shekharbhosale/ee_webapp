package com.extraaedge.web.core;

import org.testng.Reporter;


public class Action_AddNewLead_DefaultValues 
{
	public static void performAddNewLead_DefaultValues(int _tcRow) throws Exception
	{
		/*String _firstName = ExcelOperations.readCellValue(_tcRow, Constants.Username);
		Page_AddNewLead_DefaultValues.provideFirstName().sendKeys(_firstName);
		Reporter.log("User has entered First Name successfully.",true);*/
		
		Page_AddNewLead_DefaultValues.provideEmailAddress().sendKeys(EmailGenerator.randomEmailGenerator());
		Reporter.log("User has entered Email Address successfully.",true);
		
		Page_AddNewLead_DefaultValues.clickAddLeadButton().click();
		Reporter.log("User has clicked 'Add Lead' button on RHS panel",true);
		
		Thread.sleep(3000);
	}

}
