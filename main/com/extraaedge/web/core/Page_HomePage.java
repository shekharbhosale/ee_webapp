package com.extraaedge.web.core;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import com.extraaedge.web.appconfig.Constants;
import com.extraaedge.web.appconfig.ObjectMapping;

public class Page_HomePage extends AppDriver
{
	public static WebElement webElement;
	public static boolean elementVisible;
	public static ObjectMapping properties = new ObjectMapping(Constants.LOCATORS);
	
	public Page_HomePage(WebDriver appDriver) 
	{
		super(appDriver);
	}

	//Get WebElement Add Lead (Plus) icon
	public static WebElement addLeadPlusIcon()
	{
		try {
			webElement = driver.findElement(properties.selectLocator("staging_homepage_addLeadIcon"));
			Reporter.log("Add Lead (Plus) icon is displayed.",true);
		} catch (Exception e) {
			Reporter.log("No Add Lead (Plus) icon is displayed.",true);
			e.printStackTrace();
		}
		return webElement;
	}
	
	//Validate Landing Page URL after Login
	public static String homePageAfterLogin()
	{
		return driver.getCurrentUrl();
	}
	
	//Validate Landing Page URL after Logout
	public static String landingPageAfterLogout()
	{
		return driver.getCurrentUrl();
	}

	//Validate logged in User Name
	public static String VerifyLoggedInUserName()
	{
		try {
			webElement = driver.findElement(properties.selectLocator("staging_homepage_loggedInUser"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return webElement.getAttribute("title");
	}
	
	//Get WebElement Logout Link in Header
	public static WebElement logoutLink()
	{
		try {
			webElement = driver.findElement(properties.selectLocator("staging_homepage_logoutLink"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return webElement;
	}
	
	//Get WebElement Logout Link in Header
		public static boolean displayLogoutLink()
		{
			try {
				webElement = driver.findElement(properties.selectLocator("staging_homepage_logoutLink"));
			} catch (Exception e) {
				e.printStackTrace();
			}
			return elementVisible;
		}
	
	//Recent New Tab
	public static boolean displayRecentNewTab()
	{
		try {
			webElement = driver.findElement(properties.selectLocator("homepage_recentNewTab"));
			if(webElement.isDisplayed())
				return elementVisible;
		} catch (Exception e) {	
			e.printStackTrace();
		}
		return elementVisible;
	}
	
	public static boolean displayRecentNewTabCount()
	{
		try {
			webElement = driver.findElement(properties.selectLocator("homepage_recentNewTabCount"));
			if(webElement.isDisplayed())
				return elementVisible;
		} catch (Exception e) {	
			e.printStackTrace();
		}
		return elementVisible;
	}
	
	public static String extractRecentNewTabCount()
	{
		try {
			webElement = driver.findElement(properties.selectLocator("homepage_recentNewTabCount"));
			WebDriverWait wait = new WebDriverWait(driver, 30);
			wait.until(ExpectedConditions.visibilityOfElementLocated(properties.selectLocator("homepage_recentNewTabCount")));
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("New: "+webElement.getText().replaceAll("\\D+", ""));
		return webElement.getText().replaceAll("\\D+", "");
	}
	
	//Recent Online Tab
		public static boolean displayRecentOnlineTab()
		{
			try {
				webElement = driver.findElement(properties.selectLocator("homepage_recentOnlineTab"));
				if(webElement.isDisplayed())
					return elementVisible;
			} catch (Exception e) {	
				e.printStackTrace();
			}
			return elementVisible;
		}
		
		public static boolean displayRecentOnlineTabCount()
		{
			try {
				webElement = driver.findElement(properties.selectLocator("homepage_recentOnlineTabCount"));
				if(webElement.isDisplayed())
					return elementVisible;
			} catch (Exception e) {	
				e.printStackTrace();
			}
			return elementVisible;
		}
		
		public static String extractRecentOnlineTabCount()
		{
			try {
				webElement = driver.findElement(properties.selectLocator("homepage_recentOnlineTabCount"));
				WebDriverWait wait = new WebDriverWait(driver, 30);
				wait.until(ExpectedConditions.visibilityOfElementLocated(properties.selectLocator("homepage_recentOnlineTabCount")));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("Online: "+webElement.getText().replaceAll("\\D+", ""));
			return webElement.getText().replaceAll("\\D+", "");
		}
		
		//Upcoming Followup Calls Tab
			public static boolean displayUpcomingFollowupCallsTab()
			{
				try {
					webElement = driver.findElement(properties.selectLocator("homepage_upcomingfollowupTab"));
					if(webElement.isDisplayed())
						return elementVisible;
				} catch (Exception e) {	
					e.printStackTrace();
				}
				return elementVisible;
			}
			
			public static boolean displayUpcomingFollowupCallsTabCount()
			{
				try {
					webElement = driver.findElement(properties.selectLocator("homepage_upcomingfollowupTabCount"));
					if(webElement.isDisplayed())
						return elementVisible;
				} catch (Exception e) {	
					e.printStackTrace();
				}
				return elementVisible;
			}
			
			public static String extractUpcomingFollowupCallsTabCount()
			{
				try {
					webElement = driver.findElement(properties.selectLocator("homepage_upcomingfollowupTabCount"));
					WebDriverWait wait = new WebDriverWait(driver, 30);
					wait.until(ExpectedConditions.visibilityOfElementLocated(properties.selectLocator("homepage_upcomingfollowupTabCount")));
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println("UpcomingFollowupCalls: "+webElement.getText().replaceAll("\\D+", ""));
				return webElement.getText().replaceAll("\\D+", "");
			}
			
			//Upcoming Walkins Tab
			public static boolean displayUpcomingWalkinsTab()
			{
				try {
					webElement = driver.findElement(properties.selectLocator("homepage_upcomingwalkinsTab"));
					if(webElement.isDisplayed())
						return elementVisible;
				} catch (Exception e) {	
					e.printStackTrace();
				}
				return elementVisible;
			}
			
			public static boolean displayUpcomingWalkinsTabCount()
			{
				try {
					webElement = driver.findElement(properties.selectLocator("homepage_upcomingwalkinsTabCount"));
					if(webElement.isDisplayed())
						return elementVisible;
				} catch (Exception e) {	
					e.printStackTrace();
				}
				return elementVisible;
			}
			
			public static String extractUpcomingWalkinsTabCount()
			{
				try {
					webElement = driver.findElement(properties.selectLocator("homepage_upcomingwalkinsTabCount"));
					WebDriverWait wait = new WebDriverWait(driver, 30);
					wait.until(ExpectedConditions.visibilityOfElementLocated(properties.selectLocator("homepage_upcomingwalkinsTabCount")));
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println("UpcomingWalkins: "+webElement.getText().replaceAll("\\D+", ""));
				return webElement.getText().replaceAll("\\D+", "");
			}
			
			//Upcoming High Priority Tab
			public static boolean displayHighPriorityTab()
			{
				try {
					webElement = driver.findElement(properties.selectLocator("homepage_highpriorityTab"));
					if(webElement.isDisplayed())
						return elementVisible;
				} catch (Exception e) {	
					e.printStackTrace();
				}
				return elementVisible;
			}
			
			public static boolean displayHighPriorityTabCount()
			{
				try {
					webElement = driver.findElement(properties.selectLocator("homepage_highpriorityTabCount"));
					if(webElement.isDisplayed())
						return elementVisible;
				} catch (Exception e) {	
					e.printStackTrace();
				}
				return elementVisible;
			}
			
			public static String extractHighPriorityTabCount()
			{
				try {
					webElement = driver.findElement(properties.selectLocator("homepage_highpriorityTabCount"));
					WebDriverWait wait = new WebDriverWait(driver, 30);
					wait.until(ExpectedConditions.visibilityOfElementLocated(properties.selectLocator("homepage_highpriorityTabCount")));
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println("HighPriority: "+webElement.getText().replaceAll("\\D+", ""));
				return webElement.getText().replaceAll("\\D+", "");
			}
}
