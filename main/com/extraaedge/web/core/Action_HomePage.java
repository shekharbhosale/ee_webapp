package com.extraaedge.web.core;

import java.io.IOException;

import org.testng.Assert;
import org.testng.Reporter;

import com.extraaedge.web.appconfig.Constants;
import com.extraaedge.web.util.APIDataProvider;

public class Action_HomePage 
{
	public static void clickPlusIconButtonOnHomePage()
	{
		try {
			Page_HomePage.addLeadPlusIcon().click();
			Reporter.log("User has clicked Add Lead (Plus) icon.",true);
		} catch (Exception e) {
			Reporter.log("User is unable to click Add Lead (Plus) icon.",true);
			e.printStackTrace();
		}
	}
	
	public static void VerifyLoggedInUserName()
	{
		try {
			Page_HomePage.VerifyLoggedInUserName();
			Reporter.log("Verifying Logged In User.",true);
		} catch (Exception e) {
			Reporter.log("Unable to verify logged in user.",true);
			e.printStackTrace();
		}
	}
	
	public static void VerifySuccessfulLogout()
	{
		try {
			if(Page_HomePage.displayLogoutLink())
				{Reporter.log("Logout link is displayed.",true);}
				Page_HomePage.logoutLink().click();
					Reporter.log("User has clicked Log out link.",true);
			String currentPageURL = Page_HomePage.landingPageAfterLogout();
			Assert.assertEquals(Constants.PlatformURL, currentPageURL);
			Reporter.log("User has logged out successfully.",true);
		} catch (Exception e) {
			Reporter.log("Log out link is not displayed",true);
			e.printStackTrace();
		}
	}
	
	public static void VerifyHomePageURL()
	{
		try {
			String currentPageURL = Page_HomePage.homePageAfterLogin();
			Assert.assertEquals(Constants.homePageURL, currentPageURL);
			Reporter.log("User is landed on Correct Homepage. Homepage is verified.",true);
		} catch (Exception e) {
			Reporter.log("User is landed on incorrect Homepage.",true);
			e.printStackTrace();
		}
	}
	
	//New Tab
	public static void isRecentNewTabDisplayed()
	{
		try {
			Page_HomePage.displayRecentNewTab();
				Reporter.log("Recent New Tab is Displayed",true);
		} catch (Exception e) {
			Reporter.log("Recent New Tab is Not Displayed",true);
		}
	}
	
	public static void isRecentNewTabCountDisplayed()
	{
		try {
			Page_HomePage.displayRecentNewTabCount();
				Reporter.log("Recent New Tab Count is Displayed",true);
		} catch (Exception e) {
			Reporter.log("Recent New Tab Count is Not Displayed",true);
		}
	}
	
	public static void verifyRecentNewTabCount()
	{
		try {
			if(Page_HomePage.extractRecentNewTabCount().equals(APIDataProvider.getNewTabCount()))
				{Reporter.log("Recent New Tab Count displayed is correct.",true);}
		} catch (Exception e) {
			Reporter.log("Recent New Tab Count displayed is incorrect.",true);
		}
	}
	
	//Online Tab
	public static void isRecentOnlineTabDisplayed()
	{
		try {
			Page_HomePage.displayRecentOnlineTab();
				Reporter.log("Recent Online Tab is Displayed",true);
		} catch (Exception e) {
			Reporter.log("Recent Online Tab is Not Displayed",true);
		}
	}
	public static void isRecentOnlineTabCountDisplayed()
	{
		try {
			Page_HomePage.displayRecentOnlineTabCount();
				Reporter.log("Recent Online Tab Count is Displayed",true);
		} catch (Exception e) {
			Reporter.log("Recent Online Tab Count is Not Displayed",true);
		}
	}
	
	public static void verifyRecentOnlineTabCount()
	{
		try {
			if(Page_HomePage.extractRecentOnlineTabCount().equals(APIDataProvider.getOnlineTabCount()))
				{Reporter.log("Recent Online Tab Count displayed is correct.",true);}
		} catch (Exception e) {
			Reporter.log("Recent Online Tab Count displayed is incorrect.",true);
		}
	}
	
	//Upcoming Follow up Calls Tab
	public static void isUpcomingFollowupTabDisplayed()
	{
		try {
			Page_HomePage.displayUpcomingFollowupCallsTab();
				Reporter.log("Upcoming Followup Calls Tab is Displayed",true);
		} catch (Exception e) {
			Reporter.log("Upcoming Followup Calls Tab is Not Displayed",true);
		}
	}
	public static void isUpcomingFollowupTabCountDisplayed()
	{
		try {
			Page_HomePage.displayUpcomingFollowupCallsTabCount();
				Reporter.log("Upcoming Followup Calls Tab Count is Displayed",true);
		} catch (Exception e) {
			Reporter.log("Upcoming Followup Calls Tab Count is Not Displayed",true);
		}
	}
	
	public static void verifyUpcomingFollowupTabCount()
	{
		try {
			if(Page_HomePage.extractUpcomingFollowupCallsTabCount().equals(APIDataProvider.getUpcomingFollowupTabCount()))
				{Reporter.log("Upcoming Followup Calls Tab Count displayed is correct.",true);}
		} catch (Exception e) {
			Reporter.log("Upcoming Followup Calls Tab Count displayed is incorrect.",true);
		}
	}
	
	//Upcoming Walkins Tab
		public static void isWalkinsTabDisplayed()
		{
			try {
				Page_HomePage.displayUpcomingWalkinsTab();
					Reporter.log("Upcoming Walkins Tab is Displayed",true);
			} catch (Exception e) {
				Reporter.log("Upcoming Walkins Tab is Not Displayed",true);
			}
		}
		public static void isWalkinsTabCountDisplayed()
		{
			try {
				Page_HomePage.displayUpcomingWalkinsTabCount();
					Reporter.log("Upcoming Walkins Tab Count is Displayed",true);
			} catch (Exception e) {
				Reporter.log("Upcoming Walkins Tab Count is Not Displayed",true);
			}
		}
		
		public static void verifyWalkinsTabCount()
		{
			try {
				if(Page_HomePage.extractUpcomingWalkinsTabCount().equals(APIDataProvider.getUpcomingWalkinsTabCount()))
					{Reporter.log("Upcoming Walkins Tab Count displayed is correct.",true);}
			} catch (Exception e) {
				Reporter.log("Upcoming Walkins Tab Count displayed is incorrect.",true);
			}
		}

		//High Priority Tab
		public static void isHighPriorityTabDisplayed()
		{
			try {
				Page_HomePage.displayHighPriorityTab();
					Reporter.log("High Priority Tab is Displayed",true);
			} catch (Exception e) {
				Reporter.log("High Priority Tab is Not Displayed",true);
			}
		}
		public static void isHighPriorityTabCountDisplayed()
		{
			try {
				Page_HomePage.displayHighPriorityTabCount();
					Reporter.log("High Priority Tab Count is Displayed",true);
			} catch (Exception e) {
				Reporter.log("High Priority Tab Count is Not Displayed",true);
			}
		}
		
		public static void verifyHighPriorityTabCount()
		{
			try {
				if(Page_HomePage.extractHighPriorityTabCount().equals(APIDataProvider.getHighPriorityTabCount()))
					{Reporter.log("High Priority Tab Count displayed is correct.",true);}
			} catch (Exception e) {
				Reporter.log("High Priority Tab Count displayed is incorrect.",true);
			}
		}
}
