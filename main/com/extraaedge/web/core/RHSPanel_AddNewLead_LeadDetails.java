package com.extraaedge.web.core;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Reporter;

// TODO: Auto-generated Javadoc
/**
 * The Class AddNewLeadAllDetails.
 */
public class RHSPanel_AddNewLead_LeadDetails {
	

	public static WebElement element = null;

	public static WebElement addLeadIcon;

	public static WebElement firstNameBox;

	public static WebElement lastNameBox;

	public static WebElement mobileNumber;

	public static WebElement emailBox;

	public static WebElement highestQualification;

	public static WebElement campaignName;

	public static WebElement season;

	public static WebElement leadSource;

	public static WebElement leadChannel;

	public static WebElement leadPriority;

	public static WebElement leadStatus;

	public static WebElement leadReason;

	public static WebElement leadReferredTo;

	public static WebElement entity1;

	public static WebElement entity2;

	public static WebElement entity3;

	public static WebElement entity4;

	public static WebElement bestTime;

	public static WebElement leadQualified;

	public static WebElement leadContactable;

	public static WebElement isMobileVerified;

	public static WebElement countryValue;

	public static WebElement stateValue;

	public static WebElement cityValue;

	public static WebElement leadRemark;

	public static WebElement addLeadButton;
	
	/**
	 * Click add lead plus icon.
	 *
	 */
	public static void ClickAddLeadPlusIcon(WebDriver driver)
	{
		addLeadIcon = driver.findElement(By.cssSelector("#add-new-lead"));
		addLeadIcon.click();
		Reporter.log("User has clicked Add Lead (Plus) icon.",true);
	}
	
	/**
	 * Provide first name.
	 *
	 * @return the web element
	 */
	public static WebElement provideFirstName(WebDriver driver, String _firstName)
	{
		firstNameBox = driver.findElement(By.id("firstName"));
		firstNameBox.sendKeys(_firstName);
		Reporter.log("User has entered First Name successfully.",true);
		return firstNameBox;
	}
	
	/**
	 * Provide last name.
	 *
	 * @return the web element
	 */
	public static WebElement provideLastName(WebDriver driver, String _lastName)
	{
		lastNameBox = driver.findElement(By.xpath("//input[contains(@id,'lastName')]"));
		lastNameBox.sendKeys(_lastName);
		Reporter.log("User has entered Last Name successfully.",true);
		return lastNameBox;
	}
	
	/**
	 * Provide email address.
	 *
	 * @return the web element
	 */
	public static WebElement provideEmailAddress(WebDriver driver, String _email)
	{
		emailBox = driver.findElement(By.id("email"));
		emailBox.sendKeys(_email);
		Reporter.log("User has entered Email Address successfully.",true);
		return emailBox;
	}
	
	/**
	 * Provide mobile number.
	 *
	 * @return the web element
	 */
	public static WebElement provideMobileNumber(WebDriver driver, String _mobile)
	{
		mobileNumber = driver.findElement(By.xpath("//input[contains(@id,'mobile')]"));
		mobileNumber.sendKeys(_mobile);
		Reporter.log("User has entered Mobile Number successfully.",true);
		return mobileNumber;
	}
	
	/**
	 * Provide highest qualification.
	 *
	 * @return the web element
	 */
	public static WebElement provideHighestQualification(WebDriver driver, String _HQualification)
	{
		highestQualification = driver.findElement(By.xpath("//input[contains(@id,'highestQualification')]"));
		highestQualification.sendKeys(_HQualification);
		Reporter.log("User has entered Highest Qualification successfully.",true);
		return highestQualification;
	}
	
	/**
	 * Provide campaign name.
	 *
	 * @return the web element
	 */
	public static WebElement provideCampaignName(WebDriver driver, String _cmpnName)
	{
		campaignName = driver.findElement(By.xpath("//input[contains(@id,'highestQualification')]"));
		campaignName.sendKeys(_cmpnName);
		Reporter.log("User has entered Campaign Name successfully.",true);
		return campaignName;
	}
	
	/**
	 * Select season value.
	 *
	 */
	public static void selectSeasonValue(WebDriver driver)
	{
		WebElement seasonDrop = driver.findElement(By.xpath(".//*[@id='accordion']/div/div/div[7]/span/div/div/span[3]"));
		seasonDrop.click();
		season = driver.findElement(By.xpath(".//*[@id='react-select-2--option-1']"));
		season.click();
		Reporter.log("User has provided Season value.",true);
	}
	
	/**
	 * Select lead source.
	 *
	 */
	public static void selectLeadSource(WebDriver driver)
	{
		WebElement sourceDrop = driver.findElement(By.xpath(".//*[@id='accordion']/div/div/div[8]/span/div/div/span[3]"));
		sourceDrop.click();
		leadSource = driver.findElement(By.xpath(".//*[@id='react-select-3--option-1']"));
		leadSource.click();
		Reporter.log("User has provided LeadSource value.",true);
	}

	/**
	 * Select lead channel.
	 *
	 */
	public static void selectLeadChannel(WebDriver driver)
	{
		WebElement channelDrop = driver.findElement(By.xpath(".//*[@id='accordion']/div/div/div[9]/span/div/div/span[3]"));
		channelDrop.click();
		leadChannel = driver.findElement(By.xpath(".//*[@id='react-select-4--option-1']"));
		leadChannel.click();
		Reporter.log("User has provided Lead Channel value.",true);
	}
	
	/**
	 * Select lead priority.
	 *
	 */
	public static void selectLeadPriority(WebDriver driver)
	{
		WebElement priDrop = driver.findElement(By.xpath(".//*[@id='accordion']/div/div/div[10]/span/div/div/span[3]"));
		priDrop.click();
		leadPriority = driver.findElement(By.xpath(".//*[@id='react-select-5--option-1']"));
		leadPriority.click();
		Reporter.log("User has provided Lead Priority value.",true);
	}
	
	/**
	 * Select lead status.
	 *
	 */
	public static void selectLeadStatus(WebDriver driver)
	{
		WebElement priDrop = driver.findElement(By.xpath(".//*[@id='accordion']/div/div/div[11]/span/div/div/span[3]"));
		priDrop.click();
		leadStatus = driver.findElement(By.xpath(".//*[@id='react-select-6--option-0']"));
		leadStatus.click();
		Reporter.log("User has provided Lead Status value.",true);
	}
	
	/**
	 * Select lead status reason.
	 *
	 */
	public static void selectLeadStatusReason(WebDriver driver)
	{
		WebElement priDrop = driver.findElement(By.xpath(".//*[@id='accordion']/div/div/div[11]/span/div/div/span[3]"));
		priDrop.click();
		leadReason = driver.findElement(By.xpath(".//*[@id='react-select-7--option-1']"));
		leadReason.click();
		Reporter.log("User has provided Lead Status Reason value.",true);
	}
	
	/**
	 * Select referred to.
	 *
	 */
	public static void selectReferredTo(WebDriver driver)
	{
		WebElement priDrop = driver.findElement(By.xpath(".//*[@id='accordion']/div/div/div[11]/span/div/div/span[3]"));
		priDrop.click();
		leadReason = driver.findElement(By.xpath(".//*[@id='react-select-7--option-1']"));
		leadReason.click();
		Reporter.log("User has provided Lead Status Reason value.",true);
	}
	
	/**
	 * Provide lead remark.
	 *
	 * @return the web element
	 */
	public static WebElement provideLeadRemark(WebDriver driver, String _remark)
	{
		leadRemark = driver.findElement(By.xpath("//input[contains(@id,'highestQualification')]"));
		leadRemark.sendKeys(_remark);
		Reporter.log("User has entered Lead Remark successfully.",true);
		return leadRemark;
	}
	
	/**
	 * Click add lead button.
	 *
	 */
	public static void clickAddLeadButton(WebDriver driver)
	{
		addLeadButton = driver.findElement(By.id("btnAddLead"));
		addLeadButton.click();
		Reporter.log("User has clicked 'Add Lead' button on RHS panel",true);
	}

}
