package com.extraaedge.web.core;

import java.math.BigInteger;
import java.security.SecureRandom;

// TODO: Auto-generated Javadoc
/**
 * The Class emailGenerator.
 */
public class EmailGenerator {

	/** The random email. */
	public static SecureRandom randomEmail = new SecureRandom();
	
	/**
	 * Random email generator.
	 *
	 * @return the system generated dynamic email address
	 */
	public static String randomEmailGenerator()
	{
		return new BigInteger(40, randomEmail).toString(32)+"@domain.com";
	}
	
	//Use below method to generate readable, less cryptographic email address
	/*
	public String optionalEmailGenerator()
	{
		Random randomEmail = new Random();
		int randomInt = randomEmail.nextInt(9999);
		return new String("webdriver_"+randomInt+"@domain.in");
	}
	*/
	
}
