package com.extraaedge.web.core;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Reporter;

import com.extraaedge.web.appconfig.Constants;
import com.extraaedge.web.appconfig.ObjectMapping;

public class Page_AddNewLead_DefaultValues extends AppDriver {
	
	public Page_AddNewLead_DefaultValues(WebDriver appDriver) {
		super(appDriver);
		// TODO Auto-generated constructor stub
	}
	public static WebElement element = null;
	public static WebElement firstNameBox;
	public static WebElement emailBox;
	public static WebElement addLeadButton;
	public static ObjectMapping properties = new ObjectMapping(Constants.LOCATORS);	

	/*public static WebElement provideFirstName()
	{
		try {
			firstNameBox = driver.findElement(properties.selectLocator("addLead_firstNameBox"));
		} catch (Exception e) {
			Reporter.log("User has failed to enter First Name.",true);
			e.printStackTrace();
		}
		return firstNameBox;	
	}*/
	public static WebElement provideEmailAddress()
	{
		try 
		{
			emailBox = driver.findElement(properties.selectLocator("addLead_emailBox"));
		} catch (Exception e) {
			Reporter.log("User has failed to enter Email Address.",true);
		}
		return emailBox;
	}
	public static WebElement clickAddLeadButton()
	{
		try 
		{
			addLeadButton = driver.findElement(properties.selectLocator("addLead_addLeadButton"));
		} catch (Exception e) {
			Reporter.log("Add Lead Button is not clicked.",true);
		}
		return addLeadButton;
	}

}
