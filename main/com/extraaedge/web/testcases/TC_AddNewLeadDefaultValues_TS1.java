package com.extraaedge.web.testcases;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import com.extraaedge.web.appconfig.Constants;
import com.extraaedge.web.appconfig.ObjectMapping;
import com.extraaedge.web.core.AppDriver;
import com.extraaedge.web.core.Page_HomePage;
import com.extraaedge.web.core.Action_AddNewLead_DefaultValues;
import com.extraaedge.web.core.Action_HomePage;
import com.extraaedge.web.core.Action_Login;
import com.extraaedge.web.datafiledrivers.ExcelOperations;
import com.extraaedge.web.appconfig.*;

// TODO: Auto-generated Javadoc
/**
 * The Class TC_Login.
 */
public class TC_AddNewLeadDefaultValues_TS1 {
	
	public WebDriver driver;
	private String _tcName;
	private int _tcRowN=0;
//	public static ObjectMapping properties = new ObjectMapping(Constants.PROPERTY_FILE);
	
	/*
	 * Before method.
	 */
	
	@BeforeMethod
	public void _beforeTest() throws Exception
	{
		_tcName = this.toString();
		_tcName = ConfigMethods.getTCName(this.toString());
		ExcelOperations.selectExcelFile(Constants.EXCEL_PATH , "Sheet1");
		_tcRowN = ExcelOperations.getRowContains(_tcName, Constants.TCName);
		driver = ConfigMethods.OpenBrowser(_tcRowN);
		new AppDriver(driver);
	}

	/**
	 * Login to the system.
	 * @throws Exception 
	 */
	@Test (priority=1)
	public void LoginToTheSystem() throws Exception
	{
		//Snippet to read Credentials from Properties File
		/*LoginPage._userName(driver, properties.getUsername());
		LoginPage._passWord(driver, properties.getPassword());*/
		try 
		{
			Action_Login.performLoginAction(_tcRowN);
//			driver.manage().window().maximize();
		} catch (Exception e) 
		{
			throw(e);
		}
	}
	
	/*@Test (priority=2)
	public void VerifyHomePage() throws Exception
	{
		String homePageURL="";
		try {
			homePageURL = Page_HomePage.homePageAfterLogin();
				if (homePageURL.equalsIgnoreCase(Constants.homePageURL))
				{
					ExcelOperations.writeCellValue("Pass",_tcRowN, Constants.TestResult);
				}
				else if (!homePageURL.equalsIgnoreCase(Constants.homePageURL))
				{
					ExcelOperations.writeCellValue("Fail", _tcRowN, Constants.TestResult);
				}
			
			} catch (Exception e)
			{
				throw(e);
			}
		
		}*/
	
	@Test (priority=2)
	public void VerifyHomePage() throws Exception
	{
		String homePageURL="";
		try {
				LoginToTheSystem();
				homePageURL = Page_HomePage.homePageAfterLogin();
				if (homePageURL.equalsIgnoreCase(Constants.homePageURL))
					System.out.println("Correct Homepage");
			} catch (Exception e)
			{
				throw(e);
			}
		}
	
	@Test (priority = 3)
	public void AddNewLeadWithDefaultDetails() throws Exception
	{
		try 
		{
			Action_Login.performLoginAction(_tcRowN);
			Thread.sleep(2000);
			Action_HomePage.clickPlusIconButtonOnHomePage();
			Action_AddNewLead_DefaultValues.performAddNewLead_DefaultValues(_tcRowN);
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}
	/**
	 * After method.
	 */
	@AfterMethod
	public void aftermethod()
	{
		driver.close();              
	}
	
	/*
	@AfterClass
	public void afterClass()
	{
		if (driver != null) {
	          try {
	              Page_HomePage.homePageAfterLogin(); 
	          } finally {
	              driver.close(); 
	          }
	        }   
	}
	*/
}
