package com.extraaedge.web.testcases;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.Test;
import com.extraaedge.web.appconfig.Constants;
import com.extraaedge.web.appconfig.ObjectMapping;
import com.extraaedge.web.core.AppDriver;
import com.extraaedge.web.core.Page_HomePage;
import com.extraaedge.web.core.Action_HomePage;
import com.extraaedge.web.core.Action_Login;
import com.extraaedge.web.datafiledrivers.ExcelOperations;
import com.extraaedge.web.util.EmailerService;
import com.extraaedge.web.appconfig.*;

// TODO: Auto-generated Java doc
/**
 * The Class TC_Login.
 */
public class TC_Login {
	
	public WebDriver driver;
	private String _tcName;
	private int _tcRowN=0;
//	public static ObjectMapping properties = new ObjectMapping(Constants.PROPERTY_FILE);
	
	/*
	 * Before method.
	 */
	
	@BeforeSuite
	public void _BeforeSuite() throws Exception
	{
		_tcName = this.toString();
		_tcName = ConfigMethods.getTCName(this.toString());
		ExcelOperations.selectExcelFile(Constants.EXCEL_PATH , "Sheet1");
		_tcRowN = ExcelOperations.getRowContains(_tcName, Constants.TCName);
		driver = ConfigMethods.OpenBrowser(_tcRowN);
		new AppDriver(driver);
	}

	/**
	 * Login to the system.
	 * @throws Exception 
	 */
	@Test (priority=1)
	public void loginToTheSystem() throws Exception
	{
		try 
		{
			Action_Login.performLoginAction(_tcRowN);
			Thread.sleep(3000);
		} catch (Exception e) 
		{
			throw(e);
		}
	}
	
	@Test (priority=2)
	public void validateHomePageURL()
	{
		try {
			ConfigMethods.waitForPageToLoad();
			Action_HomePage.VerifyHomePageURL();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test (priority=3)
	public void validateLoggedInUserName()
	{
		try {
			Thread.sleep(3000);
			ConfigMethods.waitForPageToLoad();
			String _counselorName = Page_HomePage.VerifyLoggedInUserName();
			Assert.assertEquals(_counselorName,"shekhars" );
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test (priority=4)
	public void verifyStatusTabsDisplayedOrNot()
	{
		try {
			Action_HomePage.isRecentNewTabDisplayed();
			Action_HomePage.isRecentOnlineTabDisplayed();
			Action_HomePage.isUpcomingFollowupTabDisplayed();
			Action_HomePage.isWalkinsTabDisplayed();
			Action_HomePage.isHighPriorityTabDisplayed();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	@Test (priority=5)
	public void verifyStatusTabsCountsDisplayedOrNot()
	{
		try {
			Action_HomePage.isRecentNewTabCountDisplayed();
			Action_HomePage.isRecentOnlineTabCountDisplayed();
			Action_HomePage.isUpcomingFollowupTabCountDisplayed();
			Action_HomePage.isWalkinsTabCountDisplayed();
			Action_HomePage.isHighPriorityTabCountDisplayed();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	@Test (priority=6)
	public void validateStatusTabsCounts()
	{
		try {
			Action_HomePage.verifyRecentNewTabCount();
			Action_HomePage.verifyRecentOnlineTabCount();
			Action_HomePage.verifyUpcomingFollowupTabCount();
			Action_HomePage.verifyWalkinsTabCount();
			Action_HomePage.verifyHighPriorityTabCount();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	@Test (priority=7)
	public void validdateSuccessfulLogout()
	{
		try {
			ConfigMethods.waitForPageToLoad();
			Action_HomePage.VerifySuccessfulLogout();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	

	/**
	 * After method.
	 */
	
	@AfterClass
	public void sendTestReportOverEmail()
	{
//		EmailerService.SendTestReport();
	}
	@AfterSuite
	public void AfterSuite()
	{
		Reporter.log("Browser has been closed.");
		driver.close(); 
		
	}
	
}
