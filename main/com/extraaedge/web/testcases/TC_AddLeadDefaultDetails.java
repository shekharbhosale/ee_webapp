package com.extraaedge.web.testcases;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import com.extraaedge.web.appconfig.ConfigMethods;
import com.extraaedge.web.appconfig.Constants;
import com.extraaedge.web.core.AppDriver;
import com.extraaedge.web.core.Action_AddNewLead_DefaultValues;
import com.extraaedge.web.core.Action_HomePage;
import com.extraaedge.web.core.Action_Login;
import com.extraaedge.web.core.Page_HomePage;
import com.extraaedge.web.datafiledrivers.ExcelOperations;

// TODO: Auto-generated Java doc
/**
 * The Class TC_AddLeadDefaultDetails.
 */
public class TC_AddLeadDefaultDetails {
	
	/** The driver. */
	public WebDriver driver;
	private String _tcName;
	private int _tcRowN;

	/**
	 * Before method.
	 * @throws Exception 
	 */
	@BeforeMethod
	public void beforeMethod() throws Exception
	{
		_tcName = this.toString();
		_tcName = ConfigMethods.getTCName(this.toString());
		ExcelOperations.selectExcelFile(Constants.EXCEL_PATH , "Sheet1");
		_tcRowN = ExcelOperations.getRowContains(_tcName, Constants.TCName);
		driver = ConfigMethods.OpenBrowser(_tcRowN);
		new AppDriver(driver);
	}
	
	/**
	 * Adds the new lead with basic details.
	 * @throws Exception 
	 */
	@Test (priority = 1)
	public void AddNewLeadWithDefaultDetails() throws Exception
	{
		try 
		{
			Action_Login.performLoginAction(_tcRowN);
			Thread.sleep(2000);
			Action_HomePage.clickPlusIconButtonOnHomePage();
			Action_AddNewLead_DefaultValues.performAddNewLead_DefaultValues(_tcRowN);
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}
	
	@AfterClass
	public void afterClass()
	{
		if (driver != null) {
	          try {
	              Page_HomePage.homePageAfterLogin(); 
	          } finally {
	              driver.close(); 
	          }
	        }   
	}

}
