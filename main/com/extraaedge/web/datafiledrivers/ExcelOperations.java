package com.extraaedge.web.datafiledrivers;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import com.extraaedge.web.appconfig.Constants;

public class ExcelOperations 
{
	private static XSSFSheet sheet;
	private static XSSFWorkbook workbook;
	private static XSSFRow row;
	private static XSSFCell cell;
		
	//Set Excel File
	
	public static void selectExcelFile(String _filePath, String _sheetName) throws IOException
	{
		try {	
			//Provide excel file path
			FileInputStream fis = new FileInputStream(_filePath);
			//Select Workbook
			workbook = new XSSFWorkbook(fis);
			sheet = workbook.getSheet(_sheetName);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}	
	
	//Read Excel File
	public static String readCellValue(int rown, int coln) throws Exception
	{
		try {
			cell = sheet.getRow(rown).getCell(coln);
			String cellValue = cell.getStringCellValue();
			return cellValue;
		} catch (Exception e) 
		{
			return "";
		}
	}
	
	
	public static int getRowContains (String _tcName, int coln) throws Exception
	{
		int i=0;
		try {
			int rowCount = ExcelOperations.getLastRow();	
			for(i=0; i<rowCount; i++)
			{
				if(ExcelOperations.readCellValue(i, coln).equalsIgnoreCase(_tcName))
				{
					break;
				}
			}
			return i-1;
		} catch (Exception e) 
		{
			throw(e);
		}
	}
	
	public static int getLastRow() throws Exception
	{
		try {
			return sheet.getLastRowNum();
		} catch (Exception e) 
		{
			throw(e);
		}
	}
	
	//Write into Excel file
//	@SuppressWarnings({ "static-access", "deprecation" })
	public static void writeCellValue (String _result, int rown, int coln) throws IOException
	{
		try {
			row = sheet.getRow(rown-1);
//			cell = row.getCell(coln,row.RETURN_BLANK_AS_NULL);
			cell = row.getCell(coln);
			if(cell==null)
			{
				cell = row.createCell(coln);
				cell.setCellValue(_result);
			}
			if(!cell.equals(null))
			{
				row = sheet.getRow(rown);
				cell = row.getCell(coln);
				cell = row.createCell(coln);
				cell.setCellValue(_result);
			}
		
		FileOutputStream outputStream = new FileOutputStream(Constants.TEST_RESULT);
		workbook.write(outputStream);
//		outputStream.flush();
//		outputStream.close();
		}
		catch (Exception e) 
		{
			throw(e);
		}
	}
	
	

}
